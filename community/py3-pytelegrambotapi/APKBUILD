# Contributor: Antoine Martin (ayakael) <dev@ayakael.net>
# Maintainer: Antoine Martin (ayakael) <dev@ayakael.net>
pkgname=py3-pytelegrambotapi
pkgver=4.19.0
# sometimes upstream forgets to tag pypi releases
_gittag=$pkgver
pkgrel=0
arch="noarch"
pkgdesc="A simple, but extensible Python implementation for the Telegram Bot API."
url="https://pypi.org/project/pyTelegramBotAPI/"
license="GPL-2.0-only"
depends="py3-requests"
makedepends="
	py3-setuptools
	py3-gpep517
	py3-wheel
	py3-hatchling
	"
checkdepends="
	py3-aiohttp
	py3-pytest
	"
source="$pkgname-$_gittag.tar.gz::https://github.com/eternnoir/pyTelegramBotAPI/archive/$_gittag.tar.gz"
builddir="$srcdir"/pyTelegramBotAPI-$_gittag
subpackages="$pkgname-pyc"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer "$builddir"/.dist/*.whl
	.testenv/bin/python3 -m pytest -v
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
77e8da61b7b2b0a55eb110738e180363da3bb9c43192250e6bd486fcf08972a25269dc918f55b17bf50352e65a4c9a0a75e63b26da5c561fa5561bfe0f5294ba  py3-pytelegrambotapi-4.19.0.tar.gz
"
