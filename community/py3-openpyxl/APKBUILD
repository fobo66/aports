# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-openpyxl
_pkgreal=openpyxl
pkgver=3.1.3
pkgrel=0
pkgdesc="A Python library to read/write Excel 2010 xlsx/xlsm files"
url="https://foss.heptapod.net/openpyxl/openpyxl"
arch="noarch"
license="MIT"
depends="python3 py3-lxml py3-pandas py3-pillow"
checkdepends="py3-pytest-xdist py3-tox"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://foss.heptapod.net/openpyxl/openpyxl/-/archive/$pkgver/openpyxl-$pkgver.tar.gz"
builddir="$srcdir/$_pkgreal-$pkgver"

case "$CARCH" in
# several test fails on ppc64le | skip for now
ppc64le) options="$options !check" ;;
# some tests raise NotImplementedError on ARM, FIXME
aarch64|arm*) options="$options !check" ;;
esac

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	local ignore=
	case "$CARCH" in
		riscv64)
			ignore="$ignore --ignore=openpyxl/chart/tests/test_chart.py" # NotImplementedError
			ignore="$ignore --ignore=openpyxl/drawing/tests/test_spreadsheet_drawing.py" # FileNotFoundError
			;;
	esac
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest -n auto $ignore
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
626f1192040eee79b82f1230e0806e1a802a51477fbabfe1981c78b810944cfb34db74d38d320f3e4218f7e00f2057a08af55e31b91aacec91c2ca556f43d574  py3-openpyxl-3.1.3.tar.gz
"
